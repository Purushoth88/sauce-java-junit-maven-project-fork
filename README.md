# Sauce-Java-Sample-Working

This project is based on running the selenuim converted Junit scripts into Saucelabs. Actually this is a Java Maven project.

* The flow of this Proof of concept(POC) is like converted JSON scritps into Java Junit using selenium builder deployed into bitbucket repository, the entire java project deployed over there.
 
* To execute this project we are using jenkins, usign jenkins we are creating a job as maven in which we are mapping the bitbucket url, POM.xml and then test Suite class name as parameter in job. In the test suite we should map the class names list. 

* When we run the job it will execute the entire suite and the result file push back to bitbucket repository. 

* Finally the result will be published into Saucelabs


[Sauce Labs Dashboard](https://saucelabs.com/beta/dashboard/)

### Advice/Troubleshooting
1. It may be useful to use a Java IDE such as IntelliJ or Eclipse to help troubleshoot potential issues. 
2. There may be additional latency when using a remote webdriver to run tests on Sauce Labs. Timeouts and/or waits may need to be increased.
    * [Selenium tips regarding explicit waits](https://wiki.saucelabs.com/display/DOCS/Best+Practice%3A+Use+Explicit+Waits)

### Resources
##### [Sauce Labs Documentation](https://wiki.saucelabs.com/)

##### [SeleniumHQ Documentation](http://www.seleniumhq.org/docs/)

##### [Junit Documentation](http://junit.org/javadoc/latest/index.html)

##### [Java Documentation](https://docs.oracle.com/javase/7/docs/api/)

##### [Stack Overflow](http://stackoverflow.com/)
* A great resource to search for issues not explicitly covered by documentation.